/**************************************************************************
 *
 *      Mopdule:     ParadisInit.c
 *      Description: Contains functions for doing one-time initializations
 *                   before entering the main loop of the application.
 *
 *      Includes functions:
 *          ParadisInit()
 *
 *************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include "Home.h"
#include "Init.h"
#include "Util.h"

#ifdef PARALLEL
#include "mpi.h"
#endif


/*-------------------------------------------------------------------------
 *
 *      Function:    ParadisInit
 *      Description: Create the 'home' structure, setup timing categories
 *                   and initialize MPI.
 *
 *------------------------------------------------------------------------*/
void ParadisInit(int argc, char *argv[], Home_t **homeptr)
{
        Home_t         *home;

        home = InitHome();
        *homeptr = home;

        TimerInit(home);

#ifdef PARALLEL
        MPI_Init(&argc, &argv);
#ifdef PARADIS_IN_LIBMULTISCALE
        MPI_Comm MPI_COMM_PARADIS;
        MPI_Comm_rank(MPI_COMM_PARADIS, &home->myDomain);
        MPI_Comm_size(MPI_COMM_PARADIS, &home->numDomains);
#else
        MPI_Comm_rank(MPI_COMM_WORLD, &home->myDomain);
        MPI_Comm_size(MPI_COMM_WORLD, &home->numDomains);
#endif
#else
        home->myDomain = 0;
        home->numDomains = 1;
#endif

        TimerStart(home, TOTAL_TIME);

        TimerStart(home, INITIALIZE);
#ifdef PARADIS_IN_LIBMULTISCALE
        Initialize(home, argc, argv, MPI_COMM_PARADIS);
#else
        Initialize(home, argc, argv);
#endif
        TimerStop(home, INITIALIZE);

#ifdef PARALLEL
#ifdef PARADIS_IN_LIBMULTISCALE
        MPI_Barrier(MPI_COMM_PARADIS); /*  */
#else
        MPI_Barrier(MPI_COMM_WORLD);
#endif
#endif
        if (home->myDomain == 0) printf("ParadisInit finished\n");

        return;
}
