/**************************************************************************
 *
 *  Function    : Mobility_FCC_0
 *  Author      : Wei Cai, Seok-Woo Lee (updated 07/14/09)
 *  Description : Generic Mobility Law of FCC metals
 *                Each line has a glide plane from input
 *                and it never changes
 *                If the plane normal is not of {111} type, dislocation
 *                motion is constrained along line direction
 *                If node flag == 7, node velocity is zero
 *
 *  Returns:  0 on success
 *            1 if velcoity could not be determined
 *
 *  Modified : Jaehyun Cho jaehyun.cho@epfl.ch Description for
 *  modification : For CADD3d purpose, some new lines are added in
 *  if(param->mobfile){..}  When 'mobfile' is activated, the damping
 *  coefficients are evaluated based on data provided by the exterenal
 *  mobility files (i.e. mobility.mob) Then, the timestep is enforced
 *  to a femtosecond (1e-15s), and dislocation only moves within a
 *  femtosecond.  To do this, the timestep is multiplied on the
 *  evaluated velocity.  This is temporary fixation to avoid problems
 *  due to dynamically varied timestep of DDD.
 *  **************************************************************************/

#include "Home.h"
#include "Util.h"
#include "Mobility.h"
#include <stdio.h>
#include <math.h>

#ifdef PARALLEL
#include "mpi.h"
#endif

#ifdef PARADIS_IN_LIBMULTISCALE
#define _ENABLE_LINE_CONSTRAINT false
#else
#define _ENABLE_LINE_CONSTRAINT 1
#endif


int Mobility_FCC_0(Home_t *home, Node_t *node)
{
    int numNonZeroLenSegs = 0;
    Param_t *param = NULL;
    real8 VelxNode = 0, VelyNode = 0, VelzNode = 0, Veldotlcr = 0;
    int i = 0, j = 0, nc = 0, nconstraint = 0, nlc = 0;
    real8 normX[100], normY[100], normZ[100], normx[100], normy[100], normz[100];
    real8 lineX[100], lineY[100], lineZ[100];
    real8 a = 0, b = 0;
    real8 dx = 0, dy = 0, dz = 0, lx = 0, ly = 0, lz = 0, lr = 0, LtimesB = 0;
    real8 lcx = 0, lcy = 0, lcz = 0, normdotlc = 0;
    Node_t *nbr = NULL;
    real8 MobScrew = 0, MobEdge = 0, Mob = 0;
    real8 bv = 0, dv = 0, av = 0, vpmin = 0;
    real8 bx = 0, by = 0, bz = 0, br = 0, dangle = 0, cangle = 0, dangle_deg = 0, distance = 0;
    real8 nForce[3] = {0., 0., 0.};

    param = home->param;
    MobScrew = param->MobScrew;
    MobEdge  = param->MobEdge;
    nc = node->numNbrs;

    real8 B0 = 0,B1 = 0,B2 = 0,Ks = 0,lattice = 0,effMass = 0;
    int   idx_Fl = 0,idx_Fr = 0,idx_Al = 0,idx_Ar = 0;
    real8 sigma_p = 0,dt = 0;
    real8 LtimesB0 = 0, LtimesB1 = 0, LtimesB2 = 0;
    real8 vx_prev = 0, vy_prev = 0, vz_prev = 0;
    if(param->mobfile) {
      dt = param->timeMax;
      Ks = home->param->Ks;
      lattice = home->param->lattice;
      vx_prev = node->oldvX*dt*100.0;
      vy_prev = node->oldvY*dt*100.0;
      vz_prev = node->oldvZ*dt*100.0;
    }
      
/*
 *  If node is 'pinned' in place by constraints, or the node has any arms 
 *  with a burgers vector that has explicitly been set to be sessile (via
 *  control file inputs), the node may not be moved so just zero the velocity
 *  and return
 */
    if ((node->constraint == PINNED_NODE) ||
        NodeHasSessileBurg(home, node))
    {
        node->vX = 0.0;
        node->vY = 0.0;
        node->vZ = 0.0;
        return(0);
    }
    
/*
 *  It's possible this function was called for a node which had only zero-
 *  length segments (during SplitSurfaceNodes() for example).  If that is
 *  the case, just set the velocity to zero and return.
 */
    for (i = 0; i < nc; i++) {
        if ((nbr = GetNeighborNode(home, node, i)) == (Node_t *)NULL) continue;
        dx = node->x - nbr->x;
        dy = node->y - nbr->y;
        dz = node->z - nbr->z;
        if ((dx*dx + dy*dy + dz*dz) > 1.0e-12) {
            numNonZeroLenSegs++;
        }
    }

    if (numNonZeroLenSegs == 0) {
        node->vX = 0.0;
        node->vY = 0.0;
        node->vZ = 0.0;
        return(0);
    }


    /* copy glide plane constraints and determine line constraints */
    for(i=0;i<nc;i++)
    {
        normX[i] = normx[i] = node->nx[i];
        normY[i] = normy[i] = node->ny[i];
        normZ[i] = normz[i] = node->nz[i];

/*
 *      If needed, rotate the glide plane normals from the
 *      laboratory frame to the crystal frame.
 */
        if (param->useLabFrame) {
            real8 normTmp[3] = {normX[i], normY[i], normZ[i]};
            real8 normRot[3];

            Matrix33Vector3Multiply(home->rotMatrixInverse, normTmp, normRot);

            normX[i] = normRot[0]; normY[i] = normRot[1]; normZ[i] = normRot[2];
            normx[i] = normRot[0]; normy[i] = normRot[1]; normz[i] = normRot[2];
        }

        if ( (fabs(fabs(node->nx[i]) - fabs(node->ny[i])) > FFACTOR_NORMAL) ||
             (fabs(fabs(node->ny[i]) - fabs(node->nz[i])) > FFACTOR_NORMAL) )
        { /* not {111} plane */
            if ((nbr=GetNeighborNode(home,node,i)) == (Node_t *)NULL) {
                Fatal("Neighbor not found at %s line %d\n",__FILE__,__LINE__);
            }
            lineX[i] = nbr->x - node->x;
            lineY[i] = nbr->y - node->y; 
            lineZ[i] = nbr->z - node->z;
            ZImage (param, lineX+i, lineY+i, lineZ+i);

/*
 *          If needed, rotate the line sense from the laboratory frame to
 *          the crystal frame.
 */
            if (param->useLabFrame) {
                real8 lDir[3] = {lineX[i], lineY[i], lineZ[i]};
                real8 lDirRot[3];

                Matrix33Vector3Multiply(home->rotMatrixInverse, lDir, lDirRot);

                lineX[i] = lDirRot[0];
                lineY[i] = lDirRot[1];
                lineZ[i] = lDirRot[2];
            }
	}
	else
	{ /* no line constraint */
	    lineX[i] = lineY[i] = lineZ[i] = 0;
	}
    }
    
    /* normalize glide plane normal vectors and lc line vectors*/
    for(i=0;i<nc;i++)
    {
        a=sqrt(normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i]);
	b=sqrt(lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i]);

        if(a>0)
        {
            normX[i]/=a;
            normY[i]/=a;
            normZ[i]/=a;

            normx[i]/=a;
            normy[i]/=a;
            normz[i]/=a;
        }
        if(b>0)
        {
            lineX[i]/=b;
            lineY[i]/=b;
            lineZ[i]/=b;
        }
    }


    /* Find independent glide constraints */ 
    nconstraint = nc;
    for(i=0;i<nc;i++)
    {
        for(j=0;j<i;j++)
        {
            Orthogonalize(normX+i,normY+i,normZ+i,normX[j],normY[j],normZ[j]);
        }
        if((normX[i]*normX[i]+normY[i]*normY[i]+normZ[i]*normZ[i])<FFACTOR_ORTH)
        {
            normX[i] = normY[i] = normZ[i] = 0;
            nconstraint--;
        }
    }

/*
 *  If needed, rotate the force vector from the laboratory frame to the
 *  crystal frame
 */
    /* Find independent line constraints */
    nlc = 0;
    for(i=0;i<nc;i++)
    {
        for(j=0;j<i;j++)
        {
            Orthogonalize(lineX+i,lineY+i,lineZ+i,lineX[j],lineY[j],lineZ[j]);
        }
        if((lineX[i]*lineX[i]+lineY[i]*lineY[i]+lineZ[i]*lineZ[i])<FFACTOR_ORTH)
        {
            lineX[i] = lineY[i] = lineZ[i] = 0;
        }
        else
        {
            nlc ++;
        }
    }

    nForce[0] = node->fX;
    nForce[1] = node->fY;
    nForce[2] = node->fZ;

    /* real8 nF0_MPa,nF1_MPa,nF2_MPa; */
    /* nF0_MPa = nForce[0]/1000000; */
    /* nF1_MPa = nForce[1]/1000000; */
    /* nF2_MPa = nForce[2]/1000000; */

    if(param->mobfile){
      LtimesB0=0.0;
      LtimesB1=0.0;
      LtimesB2=0.0;
    }

    /* find total dislocation length times drag coefficent (LtimesB)*/
    for(j=0;j<nc;j++)
    {
        if ((nbr=GetNeighborNode(home,node,j)) == (Node_t *)NULL) continue;
        dx=nbr->x - node->x;
        dy=nbr->y - node->y;
        dz=nbr->z - node->z;
        ZImage (param, &dx, &dy, &dz) ;

/*
 *      If needed, rotate the line sense from the laboratory frame to
 *      the crystal frame.
 */
        if (param->useLabFrame) {
            real8 dTmp[3] = {dx, dy, dz};
            real8 dRot[3];

            Matrix33Vector3Multiply(home->rotMatrixInverse, dTmp, dRot);

            dx = dRot[0]; dy = dRot[1]; dz = dRot[2];
        }

        lr=sqrt(dx*dx+dy*dy+dz*dz);

        if (lr==0)
        { /* zero arm segment can happen after node split
           * it is OK to have plane normal vector == 0
           * Skip (do nothing)
           */
        }
        else
        {
           if((node->nx[j]==0)&&(node->ny[j]==0)&&(node->nz[j]==0))
           {
              printf("Mobility_FCC_0: (%d,%d) glide plane norm = 0\n"
                     "for segment with nonzero length lr = %e!\n",
                     node->myTag.domainID, node->myTag.index, lr);
           }

           lx=dx/lr; ly=dy/lr; lz=dz/lr;

           bx = node->burgX[j];
           by = node->burgY[j];
           bz = node->burgZ[j];
/*
 *         If needed, rotate the burgers vector from the laboratory frame to
 *         the crystal frame.
 */
           if (param->useLabFrame) {
               real8 bTmp[3] = {bx, by, bz};
               real8 bRot[3];

               Matrix33Vector3Multiply(home->rotMatrixInverse, bTmp, bRot);

               bx = bRot[0]; by = bRot[1]; bz = bRot[2];
           }

           br = sqrt(bx*bx+by*by+bz*bz);
           bx/=br; by/=br; bz/=br; /* unit vector along Burgers vector */

           dangle = fabs(bx*lx+by*ly+bz*lz);

	   if(param->mobfile){
	     dangle_deg = acos(dangle)*180.0/3.14159265359;
	     distance = fabs(90.0 - dangle_deg);
	     cangle = distance + 90.0;
	     
	     int result = 0;
	     result = EvaluateAngleRatio(home,cangle,&sigma_p,&effMass,&idx_Al,&idx_Ar);
	     if(result != 1)
	       Fatal("Error from EvaluateAngleRatio lx: %f, ly: %f, lz: %f, lr: %f, nodeX: %f, nodeY: %f, nodeZ: %f, nbrX: %f, nbrY: %f, nbrZ: %f with neighbor count: %d ",
		     lx, ly, lz ,lr, node->x, node->y, node->z, nbr->x, nbr->y, nbr->z, j);	     
	   
	     // X direction //
	     EvaluateForceRatio(home,fabs(nForce[0]/lr),  &idx_Fl,&idx_Fr);
	     EvaluateBcnst(home,cangle,fabs(nForce[0]/lr), idx_Al, idx_Ar, \
	     		   idx_Fl, idx_Fr, &B0);//, &effMass);
	     LtimesB0+=lr*B0;
	     
	     // Y direction //
	     EvaluateForceRatio(home,fabs(nForce[1]/lr),  &idx_Fl,&idx_Fr);
	     EvaluateBcnst(home,cangle,fabs(nForce[2]/lr), idx_Al, idx_Ar, \
	     		   idx_Fl, idx_Fr, &B1);//, &effMass);
	     LtimesB1+=lr*B1;
	     
	     // Z direction //
	     EvaluateForceRatio(home,fabs(nForce[2]/lr),  &idx_Fl,&idx_Fr);
	     EvaluateBcnst(home,cangle,fabs(nForce[2]/lr), idx_Al, idx_Ar, \
	     		   idx_Fl, idx_Fr, &B2);//, &effMass);
	     LtimesB2+=lr*B2;

	   } else {
	     Mob=MobEdge+(MobScrew-MobEdge)*dangle;
	     LtimesB+=(lr/Mob);
	   }

	}
    }
    if(param->mobfile){
      LtimesB0/=2.0;
      LtimesB1/=2.0;
      LtimesB2/=2.0;

      if (fabs(nForce[0]/lr) > sigma_p){
      	VelxNode =( nForce[0] - sigma_p*lr + effMass*lr*vx_prev/0.001)/(LtimesB0+effMass*lr/0.001);
      	VelxNode *= 0.01; // [nm/ps] -> [A/fs]
      	VelxNode /= dt;   // [A/fs/dt] enforce dislocation moves within
      	                  //           a femtosecond between steps
      } else {
      	VelxNode = 0.0;
      }
      if (fabs(nForce[1]/lr) > sigma_p){
      	VelyNode =(nForce[1] - sigma_p*lr + effMass*lr*vy_prev/0.001)/(LtimesB1+effMass*lr/0.001);
      	VelyNode *= 0.01;
      	VelyNode /= dt;
      } else {
      	VelyNode = 0.0;
      }
      if (fabs(nForce[2]/lr) > sigma_p){
      	VelzNode =(nForce[2] - sigma_p*lr + effMass*lr*vz_prev/0.001)/(LtimesB2+effMass*lr/0.001);
      	VelzNode *= 0.01;
      	VelzNode /= dt;
      } else {
      	VelzNode = 0.0;
      }

    } else {
      nForce[0] = node->fX;
      nForce[1] = node->fY;
      nForce[2] = node->fZ;
      /*
       *  If needed, rotate the force vector from the laboratory frame to the
       *  crystal frame
       */
      if (param->useLabFrame) {
        real8 rotForce[3];
        Matrix33Vector3Multiply(home->rotMatrixInverse, nForce, rotForce);
        VECTOR_COPY(nForce, rotForce);
      }

      /* Velocity is simply proportional to total force per unit length */
      VelxNode = nForce[0]/LtimesB;
      VelyNode = nForce[1]/LtimesB;
      VelzNode = nForce[2]/LtimesB;
    }
    
    /* Orthogonalize with glide plane constraints */
    for(i=0;i<nc;i++)
      {
        if((normX[i]!=0)||(normY[i]!=0)||(normZ[i]!=0))
	  {
	    Orthogonalize(&VelxNode,&VelyNode,&VelzNode,
			  normX[i],normY[i],normZ[i]);
	  }
      }
    
    
    /* Any dislocation with glide plane not {111} type can only move along its length
     * This rule includes LC junction which is on {100} plane
     */
#if _ENABLE_LINE_CONSTRAINT
    if(nlc==1)
      { /* only one line constraint */
        for(i=0;i<nc;i++)
	  {
            if((lineX[i]!=0)||(lineY[i]!=0)||(lineZ[i]!=0))
	      { 
   	        lcx = lineX[i];
	        lcy = lineY[i];
	        lcz = lineZ[i];
                break;
	      }
	  }
	
        /* project velocity along line */
        Veldotlcr = VelxNode*lcx+VelyNode*lcy+VelzNode*lcz;
        VelxNode = Veldotlcr*lcx;
        VelyNode = Veldotlcr*lcy;
        VelzNode = Veldotlcr*lcz;
	
	if (nconstraint<=0)
	  {	
            Fatal("MobilityLaw_FCC_0: nconstraint <= 0, nlc = 1 is impossible!");
	  }
        else if(nconstraint>=1)
	  { /* a few plane constraints and one line constraint */
            for(i=0;i<nc;i++)
	      {
		normdotlc = normx[i]*lcx + normy[i]*lcy + normz[i]*lcz;
		if(fabs(normdotlc)>FFACTOR_ORTH)
		  {
                    /* set velocity to zero if line is not on every plane */
                    VelxNode = VelyNode = VelzNode = 0;
		    break;
		  }
                else
		  {
		    /* do nothing. Skip */
		  }
	      }
	  }
      }
    else if (nlc>=2)
      {
        /* Velocity is zero when # of independnet lc constratins is equal to or more than 2 */
        VelxNode = VelyNode = VelzNode = 0;
      }
    else
      { 
        /* nlc == 0, do nothing */
      }
#endif
    
    /*
     *  If needed, rotate the velocity vector back to the laboratory frame
     *  from the crystal frame
     */
    if (param->useLabFrame) {
      real8 vTmp[3] = {VelxNode, VelyNode, VelzNode};
      real8 vRot[3];
      
      Matrix33Vector3Multiply(home->rotMatrix, vTmp, vRot);
      
      VelxNode = vRot[0];
      VelyNode = vRot[1];
      VelzNode = vRot[2];
    }
    
    node->vX = VelxNode;
    node->vY = VelyNode;
    node->vZ = VelzNode;
    
    return(0);
}
